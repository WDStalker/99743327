<?php

namespace App\Tests\Functional\Controller;

use Symfony\Bundle\FrameworkBundle\Test\WebTestCase;
use Symfony\Component\HttpFoundation\Request;

class DiscountActionTest extends WebTestCase
{
    public function test_discount_children_request_reponded_successful_test(): void
    {
        $client = static::createClient();
        $client->request(Request::METHOD_GET, 'api/discount/booking/children');

        $this->assertResponseIsSuccessful();
        $jsonResult = json_decode($client->getResponse()->getContent(), true);
        $this->assertEquals(count($jsonResult), 3);
    }

    public function test_discount_date_request_reponded_successful_test(): void
    {
        $client = static::createClient();
        $client->request(Request::METHOD_GET, 'api/discount/booking/date');

        $this->assertResponseIsSuccessful();
        $jsonResult = json_decode($client->getResponse()->getContent(), true);
        $this->assertEquals(count($jsonResult), 2);
    }
}
