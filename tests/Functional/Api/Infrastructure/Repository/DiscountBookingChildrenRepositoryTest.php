<?php

namespace App\Tests\Functional\Api\Infrastructure\Repository;

#use App\Tests\Resource\Fixture\DiscountBookingChildrenFixture;
use App\Api\Domain\Factory\DiscountBookingChildrenFactory;
use App\Api\Infrastructure\Repository\DiscountBookingChildrenRepository;
#use Faker\Factory;
#use Faker\Generator;
use Symfony\Bundle\FrameworkBundle\Test\WebTestCase;

class DiscountBookingChildrenRepositoryTest extends WebTestCase
{
    private DiscountBookingChildrenRepository $repository;

    //private Generator $faker;

    public function setUp(): void
    {
        parent::setUp();
        $this->repository = static::getContainer()->get(DiscountBookingChildrenRepository::class);
        //$this->faker = Factory::create();
    }

    public function test_discount_booking_children_added_successfully(): void
    {
        //$age = $this->faker->age();
        //$percent = $this->faker->percent();
        //$max_price = $this->faker->max_price();

        $booking_childrens = [
            ['age' => 3, 'percent' => 80, 'max_price' => 0],
            ['age' => 6, 'percent' => 30, 'max_price' => 4500],
            ['age' => 12, 'percent' => 10, 'max_price' => 0],
        ];

        foreach ($booking_childrens AS $booking_children)
        {
            //search
            if(!$this->repository->findByAge($booking_children['age'])) {

                $discount_booking_children = (new DiscountBookingChildrenFactory())->create(
                    $booking_children['age'],
                    $booking_children['percent'],
                    $booking_children['max_price']
                );

                //act
                $this->repository->add($discount_booking_children);

                //assert
                $existingDiscountBookingChildren = $this->repository->findById($discount_booking_children->getId());
                $this->assertEquals($discount_booking_children->getId(), $existingDiscountBookingChildren->getId());

            } else {
                $this->assertTrue(true);
            }

        }
    }
}
