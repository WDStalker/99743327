<?php

namespace App\Tests\Functional\Api\Infrastructure\Repository;

use App\Api\Domain\Factory\DiscountBookingDateFactory;
use App\Api\Domain\Factory\DiscountBookingDatePaymentFactory;
use App\Api\Infrastructure\Repository\DiscountBookingDatePaymentRepository;
use App\Api\Infrastructure\Repository\DiscountBookingDateRepository;
use Symfony\Bundle\FrameworkBundle\Test\WebTestCase;

class DiscountBookingDateRepositoryTest extends WebTestCase
{
    private DiscountBookingDateRepository $repository;
    private DiscountBookingDatePaymentRepository $repository_payment;

    public function setUp(): void
    {
        parent::setUp();
        $this->repository = static::getContainer()->get(DiscountBookingDateRepository::class);
        $this->repository_payment = static::getContainer()->get(DiscountBookingDatePaymentRepository::class);
    }

    public function test_discount_booking_date_added_successfully(): void
    {
        $booking_dates = [
            ['start_date' => '2027-05-01', 'price' => 10000, 'max_price' => 1500, 'payment' => [
                    ['date' => '2026-11-30', 'percent' => 7],
                    ['date' => '2026-12-31', 'percent' => 5],
                    ['date' => '2027-01-31', 'percent' => 3],
                ]
            ],
            ['start_date' => '2027-01-15', 'price' => 10000, 'max_price' => 1500, 'payment' => [
                    ['date' => '2026-08-31', 'percent' => 7],
                    ['date' => '2026-09-30', 'percent' => 5],
                    ['date' => '2026-10-31', 'percent' => 3],
                ]
            ]
        ];

        foreach ($booking_dates AS $booking_date)
        {
            $start_date = new \DateTimeImmutable($booking_date['start_date']);

            //search
            if(!$this->repository->findByStartDate($start_date)) {

                $discount_booking_date = (new DiscountBookingDateFactory())->create(
                    $start_date,
                    $booking_date['price'],
                    $booking_date['max_price']
                );

                //act
                $this->repository->add($discount_booking_date);

                //assert
                $existingDiscountBookingDate = $this->repository->findById($discount_booking_date->getId());
                $this->assertEquals($discount_booking_date->getId(), $existingDiscountBookingDate->getId());

                foreach ($booking_date['payment'] AS $booking_date_payment) {

                    $date_payment = new \DateTimeImmutable($booking_date_payment['date']);

                    $discount_booking_date_payment = (new DiscountBookingDatePaymentFactory())->create(
                        $discount_booking_date,
                        $date_payment,
                        $booking_date_payment['percent']
                    );

                    //act
                    $this->repository_payment->add($discount_booking_date_payment);

                    //assert
                    $existingDiscountBookingPaymentDate = $this->repository_payment->findById(
                        $discount_booking_date_payment->getId()
                    );
                    $this->assertEquals(
                        $discount_booking_date_payment->getId(),
                        $existingDiscountBookingPaymentDate->getId()
                    );
                }

            } else {
                $this->assertTrue(true);
            }
        }
    }
}
