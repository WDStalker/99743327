<?php

declare(strict_types=1);

namespace App\Api\Infrastructure\Controller;

use App\Api\Infrastructure\Repository\DiscountBookingChildrenRepository;
use App\Api\Infrastructure\Repository\DiscountBookingDateRepository;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;

class DiscountActionController extends AbstractController
{
    #[Route(path: '/api/discount/booking/children', name: 'api_discount_booking_children', methods: ['GET'])]
    public function DiscountBookingChildren(DiscountBookingChildrenRepository $repository_children): Response
    {
        $discount_children = $repository_children->findAll();
        return $this->json($discount_children);
    }

    #[Route(path: '/api/discount/booking/date', name: 'api_discount_booking_date', methods: ['GET'])]
    public function DiscountBookingDate(DiscountBookingDateRepository $repository_date): Response
    {
        $discount_date = $repository_date->findAll();
        return $this->json($discount_date);
    }

}