<?php

namespace App\Api\Infrastructure\Repository;

use App\Api\Domain\Entity\DiscountBookingDate;
use App\Api\Domain\Repository\DiscountBookingDateRepositoryInterface;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\Persistence\ManagerRegistry;

class DiscountBookingDateRepository
    extends ServiceEntityRepository
    implements DiscountBookingDateRepositoryInterface
{
    public function __construct(ManagerRegistry $registry)
    {
        parent::__construct($registry, DiscountBookingDate::class);
    }
    
    public function add(DiscountBookingDate $discount_booking_date): void
    {
        $this->getEntityManager()->persist($discount_booking_date);
        $this->getEntityManager()->flush();
    }

    public function findById(int $id): DiscountBookingDate
    {
        return $this->find($id);
    }

    public function findByStartDate(object $start_date): DiscountBookingDate|bool
    {
        return $this->findOneBy(['start_date' => $start_date]) ?? false;
    }
}