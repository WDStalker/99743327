<?php

namespace App\Api\Infrastructure\Repository;

use App\Api\Domain\Entity\DiscountBookingDatePayment;
use App\Api\Domain\Repository\DiscountBookingDatePaymentRepositoryInterface;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\Persistence\ManagerRegistry;

class DiscountBookingDatePaymentRepository
    extends ServiceEntityRepository
    implements DiscountBookingDatePaymentRepositoryInterface
{
    public function __construct(ManagerRegistry $registry)
    {
        parent::__construct($registry, DiscountBookingDatePayment::class);
    }
    
    public function add(DiscountBookingDatePayment $discount_booking_date_payment): void
    {
        $this->getEntityManager()->persist($discount_booking_date_payment);
        $this->getEntityManager()->flush();
    }

    public function findById(int $id): DiscountBookingDatePayment
    {
        return $this->find($id);
    }

    public function findByDate(object $date): DiscountBookingDatePayment|bool
    {
        return $this->findOneBy(['date' => $date]) ?? false;
    }
}