<?php

namespace App\Api\Infrastructure\Repository;

use App\Api\Domain\Entity\DiscountBookingChildren;
use App\Api\Domain\Repository\DiscountBookingChildrenRepositoryInterface;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\Persistence\ManagerRegistry;

class DiscountBookingChildrenRepository
    extends ServiceEntityRepository
    implements DiscountBookingChildrenRepositoryInterface
{
    public function __construct(ManagerRegistry $registry)
    {
        parent::__construct($registry, DiscountBookingChildren::class);
    }
    
    public function add(DiscountBookingChildren $discount_booking_children): void
    {
        $this->getEntityManager()->persist($discount_booking_children);
        $this->getEntityManager()->flush();
    }

    public function findById(int $id): DiscountBookingChildren
    {
        return $this->find($id);
    }

    public function findByAge(int $age): DiscountBookingChildren|bool
    {
        return $this->findOneBy(['age' => $age]) ?? false;
    }
}