<?php

namespace App\Api\Domain\Factory;

use App\Api\Domain\Entity\DiscountBookingDatePayment;

class DiscountBookingDatePaymentFactory
{

    public function create(object $start_date, object $point_date, int $percent): DiscountBookingDatePayment
    {
        return new DiscountBookingDatePayment($start_date, $point_date, $percent);
    }
}