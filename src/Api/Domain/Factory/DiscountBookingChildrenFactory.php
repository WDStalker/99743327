<?php

namespace App\Api\Domain\Factory;

use App\Api\Domain\Entity\DiscountBookingChildren;

class DiscountBookingChildrenFactory
{

    public function create(int $age, int $percent, float $max_price): DiscountBookingChildren
    {
        return new DiscountBookingChildren($age, $percent, $max_price);
    }
}