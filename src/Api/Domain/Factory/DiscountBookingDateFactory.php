<?php

namespace App\Api\Domain\Factory;

use App\Api\Domain\Entity\DiscountBookingDate;

class DiscountBookingDateFactory
{

    public function create(object $start_date, float $price, float $max_price): DiscountBookingDate
    {
        return new DiscountBookingDate($start_date, $price, $max_price);
    }
}