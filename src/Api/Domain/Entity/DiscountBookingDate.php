<?php

namespace App\Api\Domain\Entity;

use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;

class DiscountBookingDate
{
    private int $id;
    private object $discount_booking_date_payment;
    private object $start_date;
    private float $price;
    private float $max_price;

    public function __construct(object $start_date, float $price, float $max_price)
    {
        $this->start_date = $start_date;
        $this->price = $price;
        $this->max_price = $max_price;
        $this->discount_booking_date_payment = new ArrayCollection();
    }

    /**
     * @return int
     */
    public function getId(): int
    {
        return $this->id;
    }

    /**
     * @return object
     */
    public function getStartDate(): object
    {
        return $this->start_date;
    }

    /**
     * @return float
     */
    public function getPrice(): float
    {
        return $this->price;
    }

    /**
     * @return float
     */
    public function getMaxPrice(): float
    {
        return $this->max_price;
    }

    public function getPayments(): Collection
    {
        return $this->discount_booking_date_payment;
    }
}