<?php

namespace App\Api\Domain\Entity;

class DiscountBookingChildren
{
    private int $id;
    private int $age;
    private int $percent;
    private float $max_price;

    public function __construct(int $age, int $percent, float $max_price)
    {
        $this->age = $age;
        $this->percent = $percent;
        $this->max_price = $max_price;
    }

    public function getId(): int
    {
        return $this->id;
    }

    public function getAge(): int
    {
        return $this->age;
    }

    public function getPercent(): int
    {
        return $this->percent;
    }
    
    public function getMaxPrice(): float
    {
        return $this->max_price;
    }

}