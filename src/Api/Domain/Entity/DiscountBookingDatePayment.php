<?php

namespace App\Api\Domain\Entity;

class DiscountBookingDatePayment
{
    private int $id;
    private object $discount_booking_date;
    private object $point_date;
    private int $percent;

    public function __construct(object $discount_booking_date, object $point_date, int $percent)
    {
        $this->discount_booking_date = $discount_booking_date;
        $this->point_date = $point_date;
        $this->percent = $percent;
    }

    public function getId(): int
    {
        return $this->id;
    }

    public function getPointDate(): object
    {
        return $this->point_date;
    }

    public function getPercent(): int
    {
        return $this->percent;
    }


}